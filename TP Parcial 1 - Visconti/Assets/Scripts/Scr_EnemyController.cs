using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_EnemyController : MonoBehaviour
{
    public int movSpeed = 5;
    private Rigidbody rb;
    public GameObject player;
    public Scr_playerController Scr_PlayerController;

    [Header("Behaviors")]
    public bool LookAtPlayer;
    public bool MoveSideToSide;
    public bool Jump;
    public bool MoveFrontToBack;
    public bool ShootSnowballs;

    public float oscillate1;
    public float oscillate2;

    public GameObject snowballProjectile;
    public SphereCollider enemyCollider;
    public LayerMask grndLayer;

    public Rigidbody Body;
    public Rigidbody Torso;
    public Rigidbody Head;
    public Rigidbody Hat;
    Vector3 startPos;

    // Start is called before the first frame update
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody>();
        enemyCollider = Body.GetComponent<SphereCollider>();
        startPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        oscillate1 = Mathf.Cos(Time.time);
        oscillate2 = Mathf.Sin(Time.time);
        if (Scr_PlayerController.inGame == true)
        {
            rb.constraints = RigidbodyConstraints.None | RigidbodyConstraints.FreezeRotation;
            rb.isKinematic = false;
            if (LookAtPlayer == true)
            {
                transform.LookAt(new Vector3(player.transform.position.x, rb.transform.position.y, player.transform.position.z));
            }
            if (MoveSideToSide == true)
            {
                rb.AddRelativeForce(new Vector3(oscillate2*5f,0.5f,0f));
            }
            if (Jump == true)
            {
                if ((Mathf.Abs(oscillate1) < 0.1f) && (Grounded() == true))
                {
                    rb.AddForce(0f, 0.8f, 0f, ForceMode.Impulse);
                    Hat.AddForce(0f, 0.2f, 0f, ForceMode.Impulse);
                }
            }
            if (MoveFrontToBack)
            {
                rb.AddRelativeForce(new Vector3(0f, 0.5f, oscillate2*5f));
            }
            if (ShootSnowballs == true)
            {
                if (Mathf.Abs(oscillate1) < 0.1)
                {
                    GameObject snowball = Instantiate(snowballProjectile, new Vector3(rb.transform.position.x, rb.transform.position.y + 2, rb.transform.position.z), rb.transform.rotation);
                    snowball.transform.LookAt(new Vector3(player.transform.position.x, player.transform.position.y+5f, player.transform.position.z));
                    snowball.GetComponent<Rigidbody>().AddForce(snowball.transform.forward * 15f, ForceMode.Impulse);
                    Destroy(snowball, 3); //Destroy the bullet in 5 seconds
                }
            }
        } else { transform.position = startPos; rb.constraints = RigidbodyConstraints.FreezeAll; }
    }

    private bool Grounded()
    {
        return Physics.CheckCapsule(enemyCollider.bounds.center, new Vector3(enemyCollider.bounds.center.x, enemyCollider.bounds.min.y, enemyCollider.bounds.center.z), enemyCollider.radius * 0.9f, grndLayer);
    } //Returns true if the center-bottom-center (x,y,z) of the collider is hitting the ground
}
