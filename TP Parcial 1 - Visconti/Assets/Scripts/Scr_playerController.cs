using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Scr_playerController : MonoBehaviour
{
    //Technical Properties
    private Rigidbody rb;
    public GameObject playerPlaceholder;        //This GameObject stores the player global Y rotation to later change forces relative direction and camera relative rotation
    public bool inGame = false;                 //In Game bool to start tracking score
    public LayerMask grndLayer;                 //Ground Layer to reset jumps on
    public SphereCollider playerCollider;       //Player Collider to check collisions

    //Attributes
    public int defaultSpeed = 6;            //Movement Speed Variable
    public float maxSpeed = 30;             //MaxSpeed for the player
    public float jumpForce = 7.5f;          //Jump Force Modificator
        public int maxJumps = 2;            //Max Amount of jumps the player can do
        private int currJumps = 0;          //Current Amount of jumps stacked
    public int rotationSpeed = 3;           //Rotation Speed Variable

    public float sizeSpeedBuff = 1.1f;                 //Size and Speed that will be increased upon killing snowman
    public float sizeSpeedDebuff = 0.95f;                //Size and Speed that will be decreased upon hitting flags


    //Forces
    private Vector3 fwdForce;                               //Forward Force applied vector
    private Vector3 sideForce;                              //Side Force applied vector
    private Vector3 vectorRot = new(0f, 0f, 0f);    //Rotation Vector

    //Game Stats
    public float timeLeft;
    private float distanceTravelled = 0f;                   //Travelled Distance
    private Vector3 prevPosition = new(0f,0f,0f);   //Previous Position Vector (to calculate said distance)
    private int snowmans = 0;
    private int flags = 0;
    

    //UI
    public TMPro.TMP_Text gameStatus;
    string defaultStatus = "Roll Down the Hill before the time runs out!\nButcher as many snowmans as possible, avoiding the flags.\nUse WASD to Move and ArrowKeys to rotate\n(Movement is Relative to Rotation)\n\nMove to any track to start the game!\n\n";
    public TMPro.TMP_Text distanceCount;
    public TMPro.TMP_Text speedCount;
    public TMPro.TMP_Text snowmansCount;
    public TMPro.TMP_Text flagsCount;
    public TMPro.TMP_Text finalScore;


    void Start()
    {
        rb = GetComponent<Rigidbody>();                     //Set rb to self
        playerCollider = GetComponent<SphereCollider>();    //Set Player Collider to self
        gameStatus.text = defaultStatus;
    } //Variables Initialization
    void Update()
    {
        //Reset Jumps upon colliding with the ground
        if (Grounded() == true) { currJumps = maxJumps; }

        //Jump
        if (Input.GetKeyDown(KeyCode.Space) && (currJumps > 1))     //If Jump Key is pressed and there are jumps stacked available
        {
            currJumps -= 1;                                             //Remove one jump from the stack
            rb.AddForce(0f, jumpForce, 0f, ForceMode.Impulse);          //Add the Jump Force
        }

        //Retart when R is pressed or if the player fell from the map (Y position less than 0)
        if (rb.transform.position.y < 0 || Input.GetKeyDown(KeyCode.R)) { Restart(); }

        //Brakes
        if (Input.GetKey(KeyCode.LeftControl) && Grounded())        //If the Brake Key is pressed and the player is grounded
        {
            fwdForce = Vector3.zero; sideForce = Vector3.zero;          //Set Forces to Zero
            rb.angularVelocity = Vector3.zero;                          //Set Player's Angular Velocity to Zero
        }

        //Set Game Status
        if (inGame == true)
        {
            InGame();
        }

        playerPlaceholder.transform.position = rb.transform.position;   //Set Container Position to self position
    } //Player Input and Calculated Stats
    private void FixedUpdate()
    {
        float hMov = Input.GetAxis("Horizontal");                                         //Get Input for X Translation
        float vMov = Input.GetAxis("Vertical");                                           //Get Input for Z Translation


        if (Input.GetAxis("VerticalRot") != 0)                                               //If there is Rotation Input (q/e)
        { //Y Rotation
            float yRot = Input.GetAxis("VerticalRot");                                         //Get Input for Y Rotation
            vectorRot = new Vector3(0f, yRot, 0f);                                         //Update the rotation vector
            rb.transform.Rotate(vectorRot * rotationSpeed, Space.World);                    //Rotate the player transform to fit the rotation
            playerPlaceholder.transform.Rotate(vectorRot * rotationSpeed, Space.World);     //Rotate the Container to keep track of the global Y rotation
        }                                            //Calculate Movement Direction (Rotation
        if (Input.GetAxis("HorizontalRot") != 0)
        { //X Rotation
            float xRot = Input.GetAxis("HorizontalRot");                                         //Get Input for Y Rotation
            vectorRot = new Vector3(xRot, 0f, 0f);                                         //Update the rotation vector
            rb.transform.Rotate(vectorRot * rotationSpeed, Space.Self);                    //Rotate the player transform to fit the rotation
            playerPlaceholder.transform.Rotate(vectorRot * rotationSpeed, Space.Self);     //Rotate the Container to keep track of the global Y rotation
        }
        sideForce = playerPlaceholder.transform.right * defaultSpeed * hMov;              //Calculate Forward Force relative to Movement Direction
        fwdForce = (playerPlaceholder.transform.forward * defaultSpeed * vMov);           //Calculate Side Force relative to Movement Direction

        rb.AddForce(fwdForce);                                                          //Apply Forward Movement Forces
        rb.AddForce(sideForce*4);                                                         //Apply Side Movement Forces

        if (rb.velocity.magnitude > maxSpeed)                    //If the Speed is higher than 30
        {
            rb.velocity = rb.velocity.normalized * maxSpeed;     //Set the velocity to the normalized speed vector to 1, and multiply it by 30
        }                                               //Max Speed Control
    } //Calculated Movement Forces
    private void OnTriggerEnter(Collider other)
    {
        //Collision with Flags (Ragdoll)
        if (other.gameObject.tag.ToString().Contains("Flag") == true) //When starting a collision with a flag trigger
        {
            RagdollFlag(other); //Ragdoll
            TransformSizeSpeed(sizeSpeedDebuff); //Decrease Size and Speed
            flags++;
        }

        //Collision with Snowmans
        if (other.gameObject.tag.ToString().Contains("Snowman") == true)
        {
            RagdollSnowman(other); //Explode and Ragdoll
            TransformSizeSpeed(sizeSpeedBuff); //Increase Size and Speed
            snowmans++;
        }

        //Snowballs
        if (other.gameObject.tag == ("Snowball"))   //When starting a collision with a snowball
        {
            other.GetComponent<Collider>().enabled = false;
            TransformSizeSpeed(0.99f);
        }

        //Level Start
        if (other.gameObject.tag == ("LevelStart")) //When starting a colission with a Level Start trigger
        {
            inGame = true;                              //Set the state to ingame
            prevPosition = transform.position;          //Set the prevPosition to 0 to start tracking from now on
            distanceTravelled = 0f;                     //Set the DistanceTravelled to 0 to start tracking from now on
            timeLeft = 60;
        }

        //Level End
        if (other.gameObject.tag == ("LevelEnd"))   //When starting a collision with a Level End trigger
        {
            inGame = false;                             //Set the state not-ingame
            if (timeLeft > 0)
            {
                gameStatus.text = "You Won! Press [R] to restart";
                float scoreTime = (int)timeLeft * 10;
                float scoreDistance = (int)distanceTravelled / 2;
                float scoreSnowmans = snowmans * 50;
                float scoreFlags = flags * 50;
                finalScore.text = $"Time Left: " + Mathf.Round(timeLeft * 100.0f) * 0.01f + "s (+" + scoreTime + "pts)\n" +
                    "Distance Travelled: " + (int)distanceTravelled + "m (+" + scoreDistance + "pts)\n" +
                    "Butchered Snowman: " + snowmans + " (+" + scoreSnowmans + "pts)\n" +
                    "Blasted Flags: " + flags + " (-" + scoreFlags + "pts)\n" +
                    "Final Size modifier: " + rb.transform.localScale.x + "\n\n\n" +
                    "Final Score: " + (int)((scoreTime + scoreDistance + scoreSnowmans - scoreFlags)*rb.transform.localScale.x) + "pts";
            }
        }

        //Out of Level Bounds
        if (other.gameObject.tag == ("Game Over"))
        {
            Restart();
        }
    } //Collisions with Flags, Snowmans, Snowballs and Level Start Barriers

    private bool Grounded()
    {
        return Physics.CheckCapsule(playerCollider.bounds.center, new Vector3(playerCollider.bounds.center.x, playerCollider.bounds.min.y, playerCollider.bounds.center.z), playerCollider.radius * 0.9f, grndLayer);
    } //Returns true if the center-bottom-center (x,y,z) of the collider is hitting the ground
    private void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    } //Restarts the level

    private void InGame()
    {
        if (timeLeft>0)
        {
            //Distance Calculations
            Vector3 distanceVector = transform.position - prevPosition;         //Set the distanceVector to be the difference between last position and current one
            distanceTravelled += distanceVector.magnitude;                      //Add the magnitude of the distanceVector to the distance variable
            prevPosition = transform.position;                                  //Set last position to current position
            //Time Calculations
            timeLeft -= Time.deltaTime;
            //UI
            gameStatus.text = $"Time Left: \t" + Mathf.Round(timeLeft * 100.0f) * 0.01f + "s";
            speedCount.text = $""+ Mathf.Round(rb.velocity.magnitude * 100.0f) * 0.01f+"m/s";
            distanceCount.text = $"" + (int)distanceTravelled + "m";
            snowmansCount.text = $""+snowmans;
            flagsCount.text = $""+flags;
        }
        else
        {
            timeLeft = 0;
            gameStatus.text = "Time's UP! Press [R] to restart";
            inGame = false;
        }

    }

    private void RagdollFlag(Collider flag)
    {
        //Disable flag's rb constraints and freeze Y Rotation
        flag.attachedRigidbody.constraints = RigidbodyConstraints.None | RigidbodyConstraints.FreezeRotationY;

        //Add a force relative to the player's speed (a bit randomized)
        flag.attachedRigidbody.AddForce(new Vector3(Random.Range(-0.5f, 0.5f), rb.velocity.magnitude / 1000, Random.Range(-0.5f, 0.5f)));

        //Add a random torque to simulate ragdoll
        flag.attachedRigidbody.AddTorque(new Vector3(Random.Range(-0.5f, 0.5f), 0f, Random.Range(-0.5f, 0.5f)), ForceMode.Impulse);

        //Disable Flag collisions with the player
        flag.enabled = false;
    }
    private void RagdollSnowman(Collider snowman)
    {
        //Unfreeze Snowman Constraints
        snowman.attachedRigidbody.constraints = RigidbodyConstraints.None;
        //Disable Snowman Script
        snowman.GetComponent<Scr_EnemyController>().enabled = false;
        //Create a Colliders Array with all the snowman's childs
        Collider[] colliders;
        colliders = snowman.GetComponents<Collider>();
        //Make each child to not collide with the player
        foreach (Collider cl in colliders) cl.enabled = false;
        //Create a RigidBodies Array with all the snowman's childs
        Rigidbody[] rigidbodies;
        rigidbodies = snowman.GetComponentsInChildren<Rigidbody>();
        //Add Pseudo-Random Forces relative to the player's speed to the Snowman's childs so the parts explode
        foreach (Rigidbody rig in rigidbodies)
        {
            rig.isKinematic = false;
            rig.AddForce(new Vector3(
                Random.Range(-rb.velocity.magnitude / 2, rb.velocity.magnitude / 2),
                rb.velocity.magnitude / 10,
                Random.Range(-rb.velocity.magnitude / 2, rb.velocity.magnitude / 2)),
                ForceMode.Impulse);
            rig.AddTorque(new Vector3(
                Random.Range(-0.5f, 0.5f),
                0f,
                Random.Range(-0.5f, 0.5f)),
                ForceMode.Impulse);
            rig.constraints = RigidbodyConstraints.None;
        }
    }
    private void TransformSizeSpeed(float modifier)
    {
        //Modify Size
        transform.localScale = new Vector3(transform.localScale.x * modifier, transform.localScale.y * modifier, transform.localScale.z * modifier);
        //Modify Speed
        maxSpeed *= modifier;
    }
}
