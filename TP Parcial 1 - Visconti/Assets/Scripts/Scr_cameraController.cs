using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr_cameraController : MonoBehaviour
{
    public float sensibility = 5.0f;
    public float maxDistance = 3.0f;
    public float currDistance = 0f;
    private float xRotation;
    private float yRotation;

    public GameObject placeholder;
    public GameObject player;
    //public GameObject playerChar;

    public LayerMask grndLayer;

    //Properties to smooth the movement
    private Vector3 currRotation; //Current rotation vector
    private Vector3 smoothSpeed = Vector3.zero; //
    private float smoothTime = 0.5f;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetAxis("Mouse ScrollWheel") != 0)
        {
            if (maxDistance >= 1)
            {
                maxDistance -= Input.GetAxis("Mouse ScrollWheel");
            }
            if (maxDistance < 1) maxDistance = 1;
        } //Use Scrollwheel to change camera distance
        if (Input.GetKey(KeyCode.Mouse0))
        {
            float xMouse = Input.GetAxis("Mouse X") * sensibility;
            float yMouse = Input.GetAxis("Mouse Y") * sensibility;
            yRotation += xMouse;
            xRotation += yMouse;
            ApplyRotation();
        } //If the left mouse button is clicked, the camera will move with the mouse
        else
        {
            yRotation = placeholder.transform.rotation.x;
            xRotation = placeholder.transform.rotation.y;
            ApplyRotation();
        } //If not, the camera will reset to it's default position
    }

    void ApplyRotation()
    {
        xRotation = Mathf.Clamp(xRotation, -85f, 85f);
        Vector3 nextRotation = new Vector3(xRotation, yRotation);
        currRotation = Vector3.SmoothDamp(currRotation, nextRotation, ref smoothSpeed, smoothTime);
        transform.localEulerAngles = currRotation;

        bool linecast = Physics.Linecast(placeholder.transform.position+new Vector3(0f,1f,0f), transform.position-(transform.forward+new Vector3(0f,0.5f,0f)), out RaycastHit hitInfo, grndLayer);
        if (linecast == true && hitInfo.distance < maxDistance)
        {
            currDistance = hitInfo.distance;
            
        } else { currDistance = maxDistance; }

        transform.position = placeholder.transform.position - transform.forward * currDistance * player.transform.localScale.x;
        transform.position += new Vector3(0f, 1f, 0f);
    }
}